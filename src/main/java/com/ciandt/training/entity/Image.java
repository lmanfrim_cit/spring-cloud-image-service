package com.ciandt.training.entity;

public class Image {

    private Long id;
    private String description;
    private String url;


    public Image(){}


    public Image(Long id, String description, String url) {
        this.id = id;
        this.description = description;
        this.url = url;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
